/***********************************************
 * Program Name: StarMap
 * Authors: Victor Dozal, Subhakar Yarlagadda
 * Class: CSC421, GUI/OOP, 10:00 AM
 * Instructor: Dr. Weiss
 * Due Date: November 25, 2014
 * 
 * Program Description: Write a star map software program named
 * Star Map in Java using Swing. Your program should be similar 
 * in interface and functionality to programs such as Stellarium 
 * but without its complexity. Read information about bright stars 
 * from a database file. Use this data to render a view of the night 
 * sky, based on the viewer’s position at a given date/time. 
 * Allow the user to pan about the star map by clicking and 
 * dragging with the mouse. Provide the option of displaying 
 * constellations.
 * 
 * ZOOMING IN AND OUT:
 *      Left click + drag is used to pan through space.
 *      Right click + drag diagonally to top left or top right
 *         is used to zoom out.
 *      Right click + drag diagonally to bottom left or bottom right
 *         is used to zoom in.
 * 
 * Program Usage:
 *      For the program to run, you will need the following files:
 *          stars.xml
 *          constellations.xml
 *      Both files will need to be found in the root directory of
 *      the Java project.
 * 

 *      
 * Compilation Instructions - 
 *      External Libraries:
 *          An external library is used to run the program. The external
 *          library is called Piccolo2d. The JAR files are included in the
 *          ./jar folder (in relation to the project root directory).
 *          In order to run the program, you will need to add the JAR files
 *          to the project's 'Libraries'.
 *          To do this in NetBeans, do the following:
 *              On the Projects panel (left side by default), open the project
 *              in which the JAR files are to be added. Right click the 'Libraries'
 *              folder. Select the "Add JAR/Folder" option. Go to the ./jar folder,
 *              and add both JAR files contained within this directory. The
 *              two JAR files are named the following:
 *                  piccolo2d-core-1.3.1.jar
 *                  piccolo2d-extras-1.3.1.jar
 * 
 * Known Bugs - 
 *      Panning works, but it doesn't modify or altitude value. For a quick fix, we added
 *          a way for the user to modify the azimuth and altitude through the
 *          "Options -> Modify Position" option.
 *      Constellations are being overdrawn. They look very odd. It might not be a bug - further
 *      testing is required.
 *      Date and Time modifications: It looks like the date and time modifications 
 *          aren't working on a linux environment. They seemed to be working on windows - further
 *          testing is required.
 *************************************************/

package main;

import gui.FrameContainer;
import xml.file.reader.XMLFileReader;

import java.io.File;
import java.util.HashMap;

import javax.swing.JOptionPane;

/**
 *
 * @author 7023471
 */
public class Main {

	/**
	 * Main control function for the program
	 * 
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param args
	 *            the command line arguments
	 * @return nothing
	 */
	public static void main(String[] args) {
		String fileName;

		// Check if files exist in project root directory
		String filePath = filesExistInProjectRootDirectory();
                File file = new File(filePath + "/stars.xml");
		if (filePath == null || !file.exists()) {
			String msg = "Files were not found in project's root directory. Please make sure the files "
					+ " (stars.xml and constellations.xml) are placed in the project's root directory.";
			JOptionPane.showConfirmDialog(null, msg, "Error", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
			
			return;
		}

		// Ask user to open the xml files

		// Creates a new XMLFileReader
		XMLFileReader XMLReader = new XMLFileReader();

		// Initializes the location of stars.xml file name
		fileName = filePath + "/stars.xml";
		System.out.println(fileName);
		// Reads all the data from the XML file
		HashMap star_list = XMLReader.readXMLData(fileName, "star");

		// Initializes the location of constellations.xml file name
		fileName = filePath + "/constellations.xml";
		// Reads all the data from the XML file
		HashMap constellation_list = XMLReader.readXMLData(fileName, "constellation");

		// Get user input
		HashMap<String, Double> parameters = new HashMap<>();
		parameters.put("latitude", 44.08);
		parameters.put("longitude", -103.23);
		parameters.put("azi0", 90.0);
		parameters.put("alt0", 90.0);
		parameters.put("radius", 300.0);

		// Creates the frame where everything will be drawn
		new FrameContainer(star_list, constellation_list, parameters);
	}

	public static String filesExistInProjectRootDirectory() {
		File stars_xml = new File("./stars.xml");
		File constellations_xml = new File("./constellations.xml");

		if (stars_xml.exists() && !stars_xml.isDirectory() && constellations_xml.exists()
				&& !constellations_xml.isDirectory()) {
			return new File(".").getAbsolutePath();
		}

		return null;
	}
}
