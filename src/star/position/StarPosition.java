/*
 **** StarPos.java ****

 Convert star position in ra/dec
 + viewer position in lat/lon
 + current date/time
 into star position in alt/azi and x/y (orthographic projection).

 Ref: Don Teets, College Math J 38(3), pp.170-173, May 2007.

 Author: John M. Weiss, Ph.D.
 Written for CSC421/521 GUI/OOP class, SDSM&T Fall 2014.

 Modifications:
 */
package star.position;

import java.util.GregorianCalendar;

public class StarPosition {

	/**
     * User's position altitude and azimuth
     */
	private static double alt, azi;
	/**
     * Holds the "now" date that the user specified
     */
	private static GregorianCalendar now_cal;
	
	/**
	 * Sets the date that the user wants to look at the stars in
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  year the year
	 * @param  month the month
	 * @param  day the day
	 * @param  hour the hour
	 * @param  minute the minute
	 * @param  second the second
	 * @return nothing
	 */
	public static void set_now_date(int year, int month, int day, int hour, int minute, int second) {
		now_cal.set(year,  month, day, hour, minute, second);
	}

	/**
	 * Compute number of days elapsed since June 10, 2005 6:45:14 GMT
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return The number of days difference
	 */
	public static double elapsed_days() {
		// double date_now, time_now;
		// System.out.print( "Enter date and time: " );
		// cin >> date_now >> time_now;

		// e.g., suppose current time is Oct 29, 2012 11:00:00 MST
		now_cal = new GregorianCalendar();
		GregorianCalendar then_cal = new GregorianCalendar();
		now_cal.set(2012, 10, 29, 11, 0, 0);
		then_cal.set(2005, 5, 10, 6, 45, 14);

		// need current time in GMT (MST + 6 hours, or MST 7 hours if not
		// daylight savings time)
		long now_msec = now_cal.getTimeInMillis() + 6 * 3600 * 1000;
		long then_msec = then_cal.getTimeInMillis();
		double diff_days = (now_msec - then_msec) / 1000.0 / (24.0 * 3600.0);
		// System.out.println( "Diff in days = " + diff_days );
		return diff_days;
	}

	/**
	 * given observer position (lat,lon), convert star position in (ra,dec) to
	 * (azi, alt)
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  ra right ascencion
	 * @param  dec declination
	 * @param  lat latitude
	 * @param  lon longitude
	 * @return nothing
	 */
	public static void alt_azi(double ra, double dec, double lat, double lon) {
		// # days since June 10, 2005 6:45:14 GMT = 1957.093588
		double t = elapsed_days();
		// System.out.printf( "t = %.3f days elapsed\n", t );
		double tG = Math.IEEEremainder(360.0 * 1.0027379093 * t, 360.0);
		double thetaG = Math.toRadians(tG);
		// System.out.printf( "thetaG = %.3f = %.3f\n", tG, thetaG );
		double psi = tG + Math.toDegrees(lon) + 90;
		// System.out.printf( "psi = %.3f = %.3f\n", psi, Math.toRadians( psi )
		// );

		// rename ala formulas in Don's paper
		double alpha = ra;
		double beta = lat;
		double delta = dec;
		psi = Math.toRadians(psi);

		double X = Math.cos(psi) * Math.cos(delta) * Math.cos(alpha) + Math.sin(psi) * Math.cos(delta)
				* Math.sin(alpha);
		double Y = -Math.sin(beta) * Math.sin(psi) * Math.cos(delta) * Math.cos(alpha) + Math.sin(beta) * Math.cos(psi)
				* Math.cos(delta) * Math.sin(alpha) + Math.cos(beta) * Math.sin(delta);
		double Z = Math.cos(beta) * Math.sin(psi) * Math.cos(delta) * Math.cos(alpha) - Math.cos(beta) * Math.cos(psi)
				* Math.cos(delta) * Math.sin(alpha) + Math.sin(beta) * Math.sin(delta);
		// System.out.printf( "(X,Y,Z) = (%.3f,%.3f,%3f)\n\n", X, Y, Z );

		// finally compute alt/azi values
		alt = Math.atan(Z / Math.sqrt(X * X + Y * Y));
		azi = Math.acos(Y / Math.sqrt(X * X + Y * Y));
		if (X < 0.0) {
			azi = 2.0 * Math.PI - azi;
		}
	}

	/**
	 * Gets the altitude
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param none
	 * @return the altitude
	 */
	public static double getAlt() {
		return alt;
	}

	/**
	 * Gets the azimuthal
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param none
	 * @return the azimuthal
	 */
	public static double getAzi() {
		return azi;
	}
}
