package star.position;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author 7023471
 */
public class StarCalculator {

	/**
     * User's position latitude
     */
	private double latitude;
	/**
     * User's position longitude
     */
	private double longitude;
	/**
     * User's position azimuth
     */
	private double azi0;
	/**
     * User's position altitude
     */
	private double alt0;
	/**
     * distance to star: assume all stars are located on sphere of radius 300
     */
	private double radius = 300;

	/**
	 * Constructor function
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  parameters a list of parameters used to display stars
	 * @return nothing
	 */
	public StarCalculator(HashMap<String, Double> parameters) {
		latitude = parameters.get("latitude");
		longitude = parameters.get("longitude");
		azi0 = parameters.get("azi0");
		alt0 = parameters.get("alt0");
	}

	// converting star position from (ra,dec) to (azi,alt) to (x,y) screen
	// coordinates
	/**
	 * Converts a star position from right ascention and declination,
	 * to x and y coordinates
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  star_list a hashmap of stars
	 * @return A new hashmap of stars with their respective x and y coordinates
	 */
	public HashMap getStars(HashMap star_list) {
		HashMap new_star_list = new HashMap();
		Iterator star_iterator = star_list.entrySet().iterator();

		Integer count = 0;
		while (star_iterator.hasNext()) {
			Map.Entry pairs = (Map.Entry) star_iterator.next();
			HashMap star = (HashMap) pairs.getValue();

			// Get 'right ascencion' and 'declination' of the star
			String right_ascencion = (String) star.get("right_ascencion");
			String declination = (String) star.get("declination");

			// Gets the coordinates for a star, for a given right_ascencion and
			// decination
			HashMap star_coordinates = getStarCoordinates(right_ascencion, declination);
			
			// Takes bad points out
			Boolean bad_point = (Boolean)star_coordinates.get("bad_point");
			if(bad_point != null) {
				continue;
			}
			
			double x = (Double) star_coordinates.get("x");
			double y = (Double) star_coordinates.get("y");
			
			String temp = (String) star.get("visual_magnitude");
			Double vmag_temp = Double.parseDouble(temp);

			// Adds star coordinates to the list of stars
			star.put("x", x);
			star.put("y", y);
			new_star_list.put(count, star);

			star_iterator.remove(); // avoids a ConcurrentModificationException
			count++;
		}

		return new_star_list;
	}

	/**
	 * Calculates the right ascencion in radians from a right ascencion
	 * string in degrees
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  right_ascencion a string in degrees
	 * @return A double precision value containing the right ascencion in radians
	 */
	private double calculateRightAscencion(String right_ascencion) {
		// Get hr, min, sec of star's 'right_ascencion' and convert to radians
		String[] split_data = right_ascencion.split("\\s+");

		double hr = Double.parseDouble(split_data[0]);
		double min = Double.parseDouble(split_data[1]);
		double sec = Double.parseDouble(split_data[2]);

		return Math.toRadians((hr + min / 60 + sec / 3600) * 15);
	}

	/**
	 * Calculates declination in radians from a string containing the
	 * declination in degrees
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  declination a string in degrees
	 * @return A double precision value containing the declination in radians
	 */
	private double calculateDeclination(String declination) {
		// Get hr, min, sec of star's 'declination' and convert to radians
		String[] split_data = declination.split("\\s+");

		// Converts strings to doubles
		double deg = Double.parseDouble(split_data[0]);
		double min = Double.parseDouble(split_data[1]);
		double sec = Double.parseDouble(split_data[2]);
		double dec = Math.toRadians(Math.abs(deg) + min / 60 + sec / 3600);

		// Checks if deg is less than 0, if so, make it positive
		if (deg < 0) {
			dec = -dec;
		}

		return dec;
	}

	/**
	 * Gets the cordinates for a star, given the right ascencion, and the declination
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  right_ascencion a string of the right ascencion in degrees
	 * @param  declination a string of the declination in degrees
	 * @return A hashmap containing the x and y coordinates for a given star
	 */
	private HashMap getStarCoordinates(String right_ascencion, String declination) {
		// Calculate right ascencion
		double ra = calculateRightAscencion(right_ascencion);
		// Calculate declination
		double dec = calculateDeclination(declination);
		// Compute alt/azi of star
		StarPosition.alt_azi(ra, dec, latitude, longitude);
		// Get the alt and azi of star
		double alt = StarPosition.getAlt();
		double azi = StarPosition.getAzi();
		HashMap coordinates = new HashMap();

		// project star's (alt,azi) position on sphere to (x,y) coordinate on
		// viewing window
		double x = radius * Math.cos(alt) * Math.sin(azi - azi0);
		double y = radius * (Math.cos(alt0) * Math.sin(alt) - Math.sin(alt0) * Math.cos(alt) * Math.cos(azi - azi0));
		double clip = Math.sin(alt0) * Math.sin(alt) + Math.cos(alt0) * Math.cos(alt) * Math.cos(azi - azi0);

		// Puts coordinates in hashmap
		coordinates.put("x", x);
		coordinates.put("y", y);

		// Checks if a point should be clipped
		if (clip < 0.0) {
			coordinates.put("bad_point", true);
		}

		return coordinates;
	}
}
