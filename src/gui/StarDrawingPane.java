package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import star.position.StarCalculator;
import star.position.StarPosition;
import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.event.PBasicInputEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PPanEventHandler;
import edu.umd.cs.piccolo.event.PZoomEventHandler;
import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.nodes.PText;
import javax.swing.WindowConstants;


public class StarDrawingPane extends JFrame {
	/**
     * contains the list of stars
     */
	private HashMap star_list;
	/**
     * contains the list of constellations
     */
	private HashMap constellation_list;
	/**
     * contains the canvas where everything is drawn
     */
	private PCanvas canvas;
	/**
     * the constellation status. Whether it is drawn or not
     */
	private boolean consStatus = false;
	/**
     * The minimum visual magnitude
     */
	private Double min_visual_magnitude = -9999.0;
	/**
     * The object that calculates a star's position in the canvas
     */
	private StarCalculator starCalculator = null;

	/**
	 * Constructor Function
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  _star_list the list of stars
	 * @param  _constellation_list the list of constellations
	 * @param  _star_calculator calculates where the stars are positioned
	 * @return nothing
	 */
	public StarDrawingPane(HashMap _star_list, HashMap _constellation_list, StarCalculator _star_calculator) {
		// Initializes variables
		super("StarMap");
		canvas = new PCanvas();
		star_list = _star_list;
		constellation_list = _constellation_list;
		starCalculator = _star_calculator;
		// sets jframe size to 1000 x 800 pixels
		setSize(1000, 800);
                // Tell it to close on exit
                setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// Sets location relative to nothig
		setLocationRelativeTo(null);
		// Initializes app frame and canvas
		initialize();
	}

	/**
	 * Initializes the drawing pane
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return nothing
	 */
	public void initialize() {
		// Sets background and foreground colors
		canvas.setBackground(Color.BLACK);
		canvas.setForeground(Color.red);
		// Sets the event handler so that min and max scale can be specified
		PZoomEventHandler zoomEventHandler = new PZoomEventHandler();
		zoomEventHandler.setMaxScale(50);
		zoomEventHandler.setMinScale(1);
		canvas.setZoomEventHandler(zoomEventHandler);
		// Sets the pan handler so that we can turn off the auto pan
		PPanEventHandler panEventHandler = new PPanEventHandler();
		panEventHandler.setAutopan(false);
		canvas.setPanEventHandler(panEventHandler);
		// adds canvas to jframe
		add(canvas);
		// adds menu to jframe
		addMenu();
		// draws the stars on the canvas
		drawStars();
	}

	/**
	 * Adds the toolbar menu at the top of the main window
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return nothing
	 */
	public void addMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu options = new JMenu("Options");
		JMenu help_menu = new JMenu("Help");

		// Create menu items
		JMenuItem toggle_const = new JMenuItem("Toggle Constellation");
		JMenuItem min_visual_magnitude = new JMenuItem("Minimum Visual Magnitude");
		JMenuItem modify_position_menu_item = new JMenuItem("Modify Position");
		JMenuItem modify_time_menu_item = new JMenuItem("Modify Time");
		JMenuItem help_menu_item = new JMenuItem("How to Zoom in/out");
		// Add menu items
		options.add(toggle_const);
		options.add(min_visual_magnitude);
		options.add(modify_position_menu_item);
		options.add(modify_time_menu_item);

                help_menu.add(help_menu_item);
                
		// Adds all the options into the menu bar
		menuBar.add(options);
                menuBar.add(help_menu);

		// Set Accelerators
		toggle_const.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, 0));
		
                // Adds listener for when the help option is clicked
		help_menu_item.addActionListener(new ActionListener(){
                    // This is the action performed when the option is clicked
			@Override
			public void actionPerformed(ActionEvent e) {
                            String msg = "Left click + drag is used to pan through space.\n" +
                                "Right click + drag diagonally to top left or top right" +
                                " is used to zoom out.\n" +
                                "Right click + drag diagonally to bottom left or bottom right" +
                                " is used to zoom in.";
                            JOptionPane.showConfirmDialog(null, msg, "How to",
                                            JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
                            return;
                        }
                });
                
		// Adds listener for when the modify time option is clicked
		modify_time_menu_item.addActionListener(new ActionListener() {
			
			// This is the action performed when the option is clicked
			@Override
			public void actionPerformed(ActionEvent e) {
				// Initializes text fields
				JTextField year_t = new JTextField(15);
				JTextField month_t = new JTextField(15);
				JTextField day_t = new JTextField(15);
				JTextField hour_t = new JTextField(15);
				JTextField minute_t = new JTextField(15);
				JTextField second_t = new JTextField(15);
				// Creates a grid layout
				JPanel moreComplexPanel = new JPanel(new GridBagLayout());
				// Sets grid layout constraints
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.insets = new Insets(5, 5, 5, 5);
				gbc.weightx = 1.0;
				gbc.weighty = 1.0;
				
				// Creates a label and text field
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Year: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(year_t, gbc);
				
				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 1;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Month: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(month_t, gbc);

				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 2;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Day: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(day_t, gbc);

				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 3;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Hour: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(hour_t, gbc);
				
				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 4;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Minute: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(minute_t, gbc);
				
				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 5;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Second: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(second_t, gbc);
				
				// capturing button click event
				int result = JOptionPane.showConfirmDialog(null, moreComplexPanel, "Minimum Visual Magnitude",
						JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
				// If the user clicks ok
				if (result == JOptionPane.OK_OPTION) {
					// If any of the fields are empty
					if(year_t.getText().isEmpty() || month_t.getText().isEmpty() || day_t.getText().isEmpty() 
							|| hour_t.getText().isEmpty() || minute_t.getText().isEmpty() 
							|| second_t.getText().isEmpty()) {
						// Display error and do nothing
						displayErrorMsg();
						return;
					}
					// Otherwise, get the valeus for all the text fields
					int year = Integer.parseInt(year_t.getText());
					int month = Integer.parseInt(month_t.getText());
					int day = Integer.parseInt(day_t.getText());
					int hour = Integer.parseInt(hour_t.getText());
					int minute = Integer.parseInt(minute_t.getText());
					int second = Integer.parseInt(second_t.getText());
					// Sets the date within the StarPosition class
					StarPosition.set_now_date(year, month, day, hour, minute, second);
                                        StarDrawingPane.this.star_list = StarDrawingPane.this.starCalculator.getStars(star_list);
					// Draws the stars based on new date
					_drawStars();
				}
			}
		});

		// Adds listener for when the min visual magnitude option is clicked
		min_visual_magnitude.addActionListener(new ActionListener() {

			// This is the action performed when the option is clicked
			@Override
			public void actionPerformed(ActionEvent e) {
				// Initializes text fields
				JTextField min_vis_mag_text_field = new JTextField(15);

				// Creates a grid layout
				JPanel moreComplexPanel = new JPanel(new GridBagLayout());
				// Sets grid layout constraints
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.insets = new Insets(5, 5, 5, 5);
				gbc.weightx = 1.0;
				gbc.weighty = 1.0;

				// Creates a label and text field
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Minimum Visual Magnitude: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(min_vis_mag_text_field, gbc);

				// capturing button click event
				int result = JOptionPane.showConfirmDialog(null, moreComplexPanel, "Minimum Visual Magnitude",
						JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
				// If user clicks OK
				if (result == JOptionPane.OK_OPTION) {
					// If text field is empty, display error and do nothing
					if(min_vis_mag_text_field.getText().isEmpty()) {
						displayErrorMsg();
						return;
					}
					// Displays all the stars onto the dome
					StarDrawingPane.this.star_list = StarDrawingPane.this.starCalculator.getStars(star_list);
					StarDrawingPane.this.min_visual_magnitude = Double.parseDouble(min_vis_mag_text_field.getText());
					_drawStars();
				}
			}
		});

		// Adds listener for when the modify position menu option is clicked
		modify_position_menu_item.addActionListener(new ActionListener() {
			// This is the action performed when the option is clicked
			@Override
			public void actionPerformed(ActionEvent e) {

				// Initializes text fields
				JTextField latitude = new JTextField(15);
				JTextField logitude = new JTextField(15);
				JTextField azimuth = new JTextField(15);
				JTextField altitude = new JTextField(15);
				
				// Sets grid layout constraints
				JPanel moreComplexPanel = new JPanel(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.insets = new Insets(5, 5, 5, 5);
				gbc.weightx = 1.0;
				gbc.weighty = 1.0;
				
				// Creates a label and text field
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Latitude: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(latitude, gbc);
				
				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 1;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Longitude: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(logitude, gbc);

				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 2;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Azimuth: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(azimuth, gbc);

				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 3;
				gbc.anchor = GridBagConstraints.WEST;
				moreComplexPanel.add(new JLabel("Altitude: "), gbc);
				gbc.gridx = 1;
				gbc.anchor = GridBagConstraints.EAST;
				moreComplexPanel.add(altitude, gbc);

				// Creates a label and text field
				gbc.gridx = 0;
				gbc.gridy = 4;
				gbc.anchor = GridBagConstraints.WEST;
				JButton drawConstellationBtn = new JButton("Draw");
				moreComplexPanel.add(drawConstellationBtn, gbc);
                                
                                final JTextField _latitude = latitude;
                                final JTextField _logitude = logitude;
                                final JTextField _azimuth = azimuth;
                                final JTextField _altitude = altitude;

				// Sets an event for the Draw button
				drawConstellationBtn.addActionListener(new ActionListener() {
					
					// Activated once the "Draw" button is clicked
					@Override
					public void actionPerformed(ActionEvent e) {
						// Check if any field is empty
						if(_latitude.getText().isEmpty() || _logitude.getText().isEmpty() 
								|| _azimuth.getText().isEmpty() || _altitude.getText().isEmpty()) {
							// If so, display error message and do nothing
							displayErrorMsg();
							return;
						}
						
						// Otherwise, add all the text fields to the parameter variable
						HashMap<String, Double> parameters = new HashMap<>();
						parameters.put("latitude", Double.parseDouble(_latitude.getText()));
						parameters.put("longitude", Double.parseDouble(_logitude.getText()));
						parameters.put("azi0", Double.parseDouble(_azimuth.getText()));
						parameters.put("alt0", Double.parseDouble(_altitude.getText()));
						parameters.put("radius", 300.0);
						// Creates a new StarCalculator
						starCalculator = new StarCalculator(parameters);
						// Displays all the stars onto the dome
						star_list = starCalculator.getStars(star_list);
						_drawStars();
					}
				});

				// capturing button click event
				int result = JOptionPane.showConfirmDialog(null, moreComplexPanel, "Constellation Position",
						JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
				
				if (result == JOptionPane.OK_OPTION) {
					if(latitude.getText().isEmpty() || logitude.getText().isEmpty() 
							|| azimuth.getText().isEmpty() || altitude.getText().isEmpty()) {
						displayErrorMsg();
						return;
					}
					_drawStars();
				}
			}
		});

		// Adds
		toggle_const.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_drawStars();
				if (consStatus) {
					consStatus = false;
				} else {
					drawConstellations();
					consStatus = true;
				}
			}
		});

		setJMenuBar(menuBar);
	}
	
	/**
	 * Displays an error popup box stating that there are empty fields.
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return nothing
	 */
	private void displayErrorMsg() {
		String msg = "Changes did not take effect. Please do not leave empty fields.";
		JOptionPane.showConfirmDialog(null, msg, "Error",
				JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
		return;
	}

	/**
	 * Takes care of drawing the stars, and attaching an event to each star.
	 * The event attached to each star is the event that makes information boxes visible
	 * to the user, indicating star details.
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return nothing
	 */
	public void drawStars() {
		// Set size and insets
		Dimension size = getSize();
		Insets insets = getInsets();

		// Get width and height of JPanel
		int w = size.width - insets.left - insets.right;
		int h = size.height - insets.top - insets.bottom;

		// Draw every star on the JPanel
		for (int i = 0; i < star_list.size(); i++) {
			// Get a star
			HashMap star = (HashMap) star_list.get(i);
			String star_vmag = (String) star.get("visual_magnitude");

			if (Double.parseDouble(star_vmag) < min_visual_magnitude) {
				continue;
			}

			// Calculate where in the JPanel to draw the star
			float x = new Float((double) (star.get("x")) + (w / 2));
			float y = new Float((double) (star.get("y")) + (h / 2));
			// Calculate the magnitude of the star
			String visual_magnitude = (String) star.get("visual_magnitude");
			// Sets star diameter
			float diameter = 0.1f * Float.parseFloat(visual_magnitude);
			diameter += diameter * 3;
			
			// Create the star
			PPath p = PPath.createEllipse(x, y, diameter, diameter);

			String text = "";

			// The next 50+ lines check if the fields exist or not,
			// and we set the star's popup text accordingly
			text += "HRNumber: ";
			if (star.get("HRnumber") != null) {
				text += star.get("HRnumber") + "\n";
			} else {
				text += "???\n";
			}

			text += "Name: ";
			if (star.get("name") != null) {
				text += star.get("name") + "\n";
			} else {
				text += "???\n";
			}

			text += "Constellation: ";
			if (star.get("constellation") != null) {
				text += star.get("constellation") + "\n";
			} else {
				text += "???\n";
			}

			text += "Right Ascencion: ";
			if (star.get("right_ascencion") != null) {
				text += star.get("right_ascencion") + "\n";
			} else {
				text += "???\n";
			}

			text += "Declination: ";
			if (star.get("declination") != null) {
				text += star.get("declination") + "\n";
			} else {
				text += "???\n";
			}

			text += "Visual Magnitude: ";
			if (star.get("Visual Magnitude") != null) {
				text += star.get("visual_magnitude") + "\n";
			} else {
				text += "???\n";
			}

			text += "Star Class: ";
			if (star.get("star_class") != null) {
				text += star.get("star_class") + "\n";
			} else {
				text += "???\n";
			}

			text += "Common Name: ";
			if (star.get("common_name") != null) {
				text += star.get("common_name") + "\n";
			} else {
				text += "???\n";
			}

			p.setName(text);
                        final PPath _p = p;
			p.addInputEventListener(new PBasicInputEventHandler() {
				// Activates when mouse is clicked on a star element
				@Override
				public void mousePressed(PInputEvent aEvent) {
					super.mousePressed(aEvent);
				}

				PText ptext;

				// Sets action for when mouse enters a star element
				@Override
				public void mouseEntered(PInputEvent aEvent) {
					super.mouseEntered(aEvent);
					// Finds picked node
					PPath ppt = (PPath) aEvent.getPickedNode();
					// JOptionPane.showMessageDialog(null, name);
					ptext = new PText(ppt.getName());
					// Sets popup text ocolor
					ptext.setPaint(Color.green);
					// Add text to canvas
					ptext.setBounds(aEvent.getCanvasPosition().getX(), aEvent.getCanvasPosition().getY(), 40, 50);
					canvas.getLayer().addChild(ptext);

					_p.repaint();

				}

				// Sets action for when mouse leaves a star element
				public void mouseExited(PInputEvent aEvent) {
					super.mouseExited(aEvent);
					// Removes the text element
					canvas.getLayer().removeChild(ptext);
				}
			});

			p.setStrokePaint(Color.GRAY);
			canvas.getLayer().addChild(p);
		}

	}
	
	/**
	 * Method to be called every time we want to draw stars. Removes all the stars
	 * and repaints them.
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return nothing
	 */
	private void _drawStars() {
		canvas.getLayer().removeAllChildren();
		drawStars();
	}

	/**
	 * Draws the constellations onto the main window
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  none
	 * @return nothing
	 */
	private void drawConstellations() {
		// For each constellation
		for (int i = 0; i < constellation_list.size(); i++) {
			// Get the constellation
			HashMap constellation = (HashMap) constellation_list.get(i);
			ArrayList<String> constellation_lines = (ArrayList) constellation.get("lines");

			// Go through each of the lines in the constellation
			for (int j = 0; j < constellation_lines.size(); j++) {
				// Get the star names
				String[] lines = constellation_lines.get(j).split(" ");
				HashMap<Float, Float> from = parseStarNames(lines[1]);
				HashMap<Float, Float> to = parseStarNames(lines[3]);
				// Find the coordinates for stars
				// Add the coordinates to the hashmap of star coordinates

				if (from.keySet().isEmpty()) {
					continue;
				}
				if (to.keySet().isEmpty()) {
					continue;
				}

				float fromx = from.keySet().iterator().next();
				float fromy = from.get(fromx);
				float tox = to.keySet().iterator().next();
				float toy = to.get(tox);
				PPath connste = PPath.createLine(fromx, fromy, tox, toy);
				connste.setPaint(Color.RED);
				connste.setStrokePaint(Color.RED);
				// PInterpolatingActivity
				// act=connste.animateToColor(Color.WHITE,0);
				canvas.getLayer().addChild(connste);
			}
		}

		// Construct a HashMap of the points in the constellation
	}

	/**
	 * Parses a star name, and finds its coordinate within the list of stars.
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param  starName the name of the star to parse and find coordinates
	 * @return A hashmap of coordinates. Key of hashmap is x coordinate, value is y coordinate
	 */
	private HashMap<Float, Float> parseStarNames(String starName) {
		HashMap<Float, Float> coord = new HashMap<>();
		Insets insets = getInsets();
		// Set size and insets
		Dimension size = getSize();
		// Get width and height of JPanel
		int w = size.width - insets.left - insets.right;
		int h = size.height - insets.top - insets.bottom;
		for (int i = 0; i < star_list.size(); i++) {
			// Get a star
			HashMap star = (HashMap) star_list.get(i);
			// Calculate where in the JPanel to draw the star
			float x = new Float((double) (star.get("x")) + (w / 2));
			float y = new Float((double) (star.get("y")) + (h / 2));
			String common_name = (String) star.get("common_name");
			String name = (String) star.get("name");

			// If name or common name exist, and either of them are equal to the star name we are trying to search
			// Then put the coordinate in
			if ((name != null && name.equals(starName)) || (common_name != null && common_name.equals(starName))) {
				coord.put(x, y);
				break;
			}
		}
		return coord;
	}

}
