package gui;

import java.util.HashMap;
import javax.swing.*;
import star.position.StarCalculator;

/**
 *
 * @author 7023471
 */
public final class FrameContainer {

	/**
     * Contains a list of stars
     */
	HashMap star_list;
	/**
     * Contains the list of constellations
     */
	HashMap constellation_list;

	/**
	 * Constructor function
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param _star_list hashmap of stars
	 * @param _constellation_list hashmap of constellations
	 * @param parameters hashmap containing parameters to figure out position of stars
	 * @return nothing
	 */
	public FrameContainer(HashMap _star_list, HashMap _constellation_list, HashMap<String, Double> parameters) {
		// Creates a new StarCalculator
		StarCalculator starDisplay = new StarCalculator(parameters);
		// Displays all the stars onto the dome
		star_list = starDisplay.getStars(_star_list);
		constellation_list = _constellation_list;
		// Draw on the frame
		drawOnFrame(starDisplay);
	}

	/**
	 * Draws the stars on the drawing pane
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param star_calculator the object with which we calculate a star's position
	 * @return nothing
	 */
	public void drawOnFrame(StarCalculator star_calculator) {
		// Set frame's content to be the panel to draw on
		// and draw on it
            
                final StarCalculator _star_calculator = star_calculator;
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new StarDrawingPane(star_list, constellation_list, _star_calculator).setVisible(true);
			}
		});

	}
}
