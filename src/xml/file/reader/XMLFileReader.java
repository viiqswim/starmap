package xml.file.reader;

/**
 *
 * @author 7023471
 */
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class XMLFileReader {

	/**
	 * Reads the XML data from the specified file, and with the
	 * method specified by the tagName
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param fileName a string containing the path to the file
	 * @param tagName a string containing which method to use while reading XML file
	 * @return A hashmap containing the information read from the XML file
	 */
	public HashMap readXMLData(String fileName, String tagName) {
		HashMap item_list = new HashMap();

		try {
			// Open XML file
			File fXmlFile = new File(fileName);
			// Build and parse the XML file
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// Normalize the document
			doc.getDocumentElement().normalize();

			// Get all 'star' nodes
			NodeList nList = doc.getElementsByTagName(tagName);
			Integer count = nList.getLength();

			// Iterate over all the 'star' nodes
			for (int i = 0; i < nList.getLength(); i++) {
				Node nNode = nList.item(i);
				// Check that the item we retrieved is an element node
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					// Cast the node to an element
					Element eElement = (Element) nNode;
					if (tagName.equals("star")) {
						// Get the current star information
						HashMap star = getStarInfo(eElement);
						// Add the star to our list of stars
						item_list.put(i, star);
					}
					if (tagName.equals("constellation")) {
						// Get the current constellation information
						HashMap constellation = getConstellationInfo(eElement);
						// Add the constellation to our list of stars
						// Key: integer from 0 to number of constellations
						// Value: a hashmap containing constellation information
						item_list.put(i, constellation);
					}
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		// Return the list of stars
		return item_list;
	}

	/**
	 * Method to read star information from an XML document
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param eElement the element containing a star element
	 * @return A hashmap containing a star
	 */
	private HashMap getStarInfo(Element eElement) {
		HashMap star = new HashMap();
		String HRnumber = null;
		String name = null;
		String constellation = null;
		String right_ascencion = null;
		String declination = null;
		String visual_magnitude = null;
		String star_class = null;
		String common_name = null;

		// Checks if there a tag in the xml with a given name, and then
		// Gets the elements found within a star and puts them in a string
		if (eElement.getElementsByTagName("HRnumber").getLength() != 0) {
			HRnumber = eElement.getElementsByTagName("HRnumber").item(0).getTextContent();
		}
		if (eElement.getElementsByTagName("name").getLength() != 0) {
			name = eElement.getElementsByTagName("name").item(0).getTextContent();
		}
		if (eElement.getElementsByTagName("constellation").getLength() != 0) {
			constellation = eElement.getElementsByTagName("constellation").item(0).getTextContent();
		}
		if (eElement.getElementsByTagName("ra").getLength() != 0) {
			right_ascencion = eElement.getElementsByTagName("ra").item(0).getTextContent();
		}
		if (eElement.getElementsByTagName("dec").getLength() != 0) {
			declination = eElement.getElementsByTagName("dec").item(0).getTextContent();
		}
		if (eElement.getElementsByTagName("vmag").getLength() != 0) {
			visual_magnitude = eElement.getElementsByTagName("vmag").item(0).getTextContent();
		}
		if (eElement.getElementsByTagName("class").getLength() != 0) {
			star_class = eElement.getElementsByTagName("class").item(0).getTextContent();
		}

		// Checks if there is a 'common_name' xml tag
		if (eElement.getElementsByTagName("common_name").getLength() != 0) {
			common_name = eElement.getElementsByTagName("common_name").item(0).getTextContent();
		}

		// Puts the strings in a star hashmap.
		// Removes useless spaces from each string
		// Also, it checks if the tag exists before putting it in.
		if (common_name != null) {
			star.put("HRnumber", cleanUpSpaces(HRnumber));
		}
		if (name != null) {
			star.put("name", cleanUpSpaces(name));
		}
		if (constellation != null) {
			star.put("constellation", cleanUpSpaces(constellation));
		}
		if (right_ascencion != null) {
			star.put("right_ascencion", cleanUpSpaces(right_ascencion));
		}
		if (declination != null) {
			star.put("declination", cleanUpSpaces(declination));
		}
		if (visual_magnitude != null) {
			star.put("visual_magnitude", cleanUpSpaces(visual_magnitude));
		}
		if (star_class != null) {
			star.put("star_class", cleanUpSpaces(star_class));
		}
		// Checks if the common_name tag existed
		if (common_name != null) {
			star.put("common_name", cleanUpSpaces(common_name));
		}

		return star;
	}

	/**
	 * Method to read constellation information from an XML document
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param eElement the element containing a constellation element
	 * @return A hashmap containing a constellation
	 */
	private HashMap getConstellationInfo(Element eElement) {
		HashMap constellation = new HashMap();

		// Get the name element's text
		String name = eElement.getElementsByTagName("name").item(0).getTextContent();
		;
		// Get the abbreviation element's text
		String abbreviation = eElement.getElementsByTagName("abbr").item(0).getTextContent();
		// Clean up the spaces for both the name and abbreviation fields
		constellation.put("name", cleanUpSpaces(name));
		constellation.put("abbreviation", cleanUpSpaces(abbreviation));

		// Get all the elements that have a "line" tag name
		NodeList lineNodes = eElement.getElementsByTagName("line");
		// Array to hold all the lines
		ArrayList lineList = new ArrayList();

		// Iterate over the list of lines
		for (int i = 0; i < lineNodes.getLength(); i++) {
			// Get a line from the xml
			Node line = lineNodes.item(i);
			// Convert line into element so that we can get its text content
			Element lineElement = (Element) line;
			String content = lineElement.getTextContent();
			// Add the content of the line to the list of lines
			lineList.add(content);
		}

		// Add all the lines to the constellation
		constellation.put("lines", lineList);

		return constellation;
	}

	/**
	 * Method to get a child element with a given name
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param list NodeList containing all the child elements within an XML node
	 * @param name the XML node to find
	 * @return A node containing the element we are trying to find
	 */
	Node getChild(final NodeList list, final String name) {
		for (int i = 0; i < list.getLength(); i++) {
			final Node node = list.item(i);
			if (name.equals(node.getNodeName())) {
				return node;
			}
		}
		return null;
	}

	/**
	 * Gets a string and cleans up the spaces at the beginning and end of the string
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param str the string to clean up spaces from
	 * @return The cleaned up string
	 */
	private String cleanUpSpaces(String str) {
		// If the first character is a space, remove it.
		if (str.charAt(0) == ' ') {
			str = deleteCharAt(str, 0);
		}
		// If the last character is a space, remove it
		if (str.charAt(str.length() - 1) == ' ') {
			str = deleteCharAt(str, str.length() - 1);
		}

		return str;
	}

	/**
	 * Deletes a character from a string at a given position
	 * @author Victor Dozal, Subhakar yarlagadda
	 * @param strValue the string to delete an element from
	 * @param index the index to delete the character at
	 * @return A string with the deleted character
	 */
	private static String deleteCharAt(String strValue, int index) {
		return strValue.substring(0, index) + strValue.substring(index + 1);
	}
}